
package cubo;

import com.sun.j3d.utils.geometry.ColorCube;
import com.sun.j3d.utils.universe.SimpleUniverse;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
/**
 *
 * @author dairininya@gmail.com
 */
public class Cubo implements Runnable{
    SimpleUniverse universo = new SimpleUniverse();
    BranchGroup grupo = new BranchGroup();
    ColorCube cubo = new ColorCube(0.3);
    TransformGroup GT = new TransformGroup();
    Transform3D transformar = new Transform3D();
    Thread hilo1 = new Thread(this);
    
    double y=0;

    /**
     * @param args the command line arguments
     */
    public Cubo(){
        GT.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        hilo1.start();
        transformar.rotY(1.9);
        transformar.rotX(1);

        GT.setTransform(transformar);
        GT.addChild(cubo);
        grupo.addChild(GT);
        universo.getViewingPlatform().setNominalViewingTransform();
        universo.addBranchGraph(grupo);
        
    } 
    
    public static void main(String[] args) {
        new Cubo();  
     }
    
    public void run(){
        Thread ct = Thread.currentThread();
        while(ct==hilo1){
            try{
                y=y+0.1;
                transformar.rotY(y);
                GT.setTransform(transformar);
                Thread.sleep(100);
            }catch(InterruptedException ex){
                Logger.getLogger(Cubo.class.getName()).log(Level.SEVERE,null,ex);
            }
        }
    }
    
}
